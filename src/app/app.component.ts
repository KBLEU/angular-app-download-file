import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { catchError, throwError } from 'rxjs';

const URL = 'http://192.168.1.5:3010/downloadzip';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'angular-webapp';

  // Injection du client http
  constructor(private http: HttpClient) {}

  // fonction qui récupèrent les erreurs d'appels HTTP
  handleError(error: HttpErrorResponse) {
    console.log('error ', error);
    return throwError(
      () => new Error("Une erreur est survenue lors l'appel http.")
    );
  }

  // fonction qui permet le téléchargement du fichier
  downloadFile() {
    console.log('i clicked on download file...');
    this.http
      .get(URL, {
        responseType: 'blob',
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/zip',
          Accept: 'application/zip, text/plain, */*',
        },
      })
      .pipe(catchError(this.handleError))
      .subscribe((blob) => {
        console.log(blob);
        const link = document.createElement('a');
        var url = window.URL || window.webkitURL;
        link.download = 'mon_fichier.zip';
        link.href = url.createObjectURL(blob);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      });
  }
}
